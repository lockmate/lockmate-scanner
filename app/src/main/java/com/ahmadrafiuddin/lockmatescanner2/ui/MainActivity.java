package com.ahmadrafiuddin.lockmatescanner2.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.ahmadrafiuddin.lockmatescanner2.R;
import com.ahmadrafiuddin.lockmatescanner2.model.QrKey;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    private TextView mTextView;
    private List<QrKey> validKeys;
    private static String jsonUrl = "http://gmm-student.fc.utm.my/~arbna/lockmate/get_keys.php";
    private IntentIntegrator integrator = new IntentIntegrator(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextView = (TextView) findViewById(R.id.lbl_text);

        validKeys = new ArrayList<>();
        downloadValidKeys();

    }

    private void downloadValidKeys() {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(jsonUrl).build();
                try {
                    Response response = client.newCall(request).execute();

                    JSONArray array = new JSONArray(response.body().string());

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject singleKeyJson = array.getJSONObject(i);

                        QrKey singleKey = new QrKey(singleKeyJson.getString("id"), singleKeyJson.getString("studentName"), singleKeyJson.getString("matricNo"), singleKeyJson.getString("password"));
                        validKeys.add(singleKey);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    System.out.println("End of content");
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                integrator.setPrompt("");
                integrator.setCameraId(0);
                integrator.setBeepEnabled(true);
                integrator.setBarcodeImageEnabled(false);
                integrator.setOrientationLocked(false);
                integrator.initiateScan();
            }
        };

        task.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                promptForExit();
            }
            else {
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

                alertDialog.setMessage(getString(R.string.invalid_qr_code_message));

                for (int i = 0; i < validKeys.size(); i++) {
                    if (result.getContents().equals(validKeys.get(i).getPassword())) {
                        // Scanned QR code is in Database
                        alertDialog.setMessage(getString(R.string.valid_qr_code_message));
                        break;
                    }
                }

                alertDialog.setTitle(getString(R.string.qr_code_scanned_message_title));
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        integrator.initiateScan();
                    }
                });
                alertDialog.show();
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        promptForExit();
    }

    private void promptForExit() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                integrator.initiateScan();
            }
        });

        alertDialog.setMessage("Do you want to exit?");
        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
}
